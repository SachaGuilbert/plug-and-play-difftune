## This repo is a copy of the original DiffTune+ repo, but with improvements for Linux users and made much more plug and play

Original repo: https://github.com/Sheng-Cheng/DiffTuneOpenSource/tree/main

See the original repo's README to get started.
