% PID contoller for drive train system
% X is position and velocity, dXdt is the derivative of X, Xsum is the sum of X(not sum of error, still works), Xref is the refference and k_vec is P, I and D parameters.

function [P,u,err_m] = controller(X,dXdt,err,k_vec,prev_P,dt,parameters,err_m_sum)
    Kp_m = k_vec(1);
    Ki_m = k_vec(2);
    Kp_l = k_vec(3);
    J_m = parameters(4);

    P = Kp_l * err(4);
    dPdt = (P-prev_P)/dt;
    err_m = P - X(1);
    u = Kp_m*err_m + Ki_m*err_m_sum + J_m*dPdt;
