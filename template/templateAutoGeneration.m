% This script uses CasADi to autogenerate functions for online Jacobian
% evalutaion

clear all
addpath('~/programs/fagprojekt/casADI');
import casadi.*

%% define the dimensions
dim_state = 4; % dimension of system state
dim_control = 1;  % dimension of control inputs
dim_controllerParameters = 3;  % dimension of controller parameters

%% load constant physical parameters, e.g., mass or sample time
% m = MX.sym('m',1);  % mass
parameters = MX.sym('parameters',8);

T_l = MX.sym('T_l',1);
T_fm = MX.sym('T_fm',1);
T_fl = MX.sym('T_fl',1);

omega_m = MX.sym('omega_m',1);
theta_m = MX.sym('theta_m',1);
omega_l = MX.sym('omega_l',1);
theta_l = MX.sym('theta_l',1);

err = MX.sym('err',dim_state);
prev_P = MX.sym('prev_P',1);
err_m_sum = MX.sym('err_m_sum',1);
% prev_P = MX.sym('prev_P',1);
% prev_P = MX.sym('prev_P',1);
% prev_P = MX.sym('prev_P',1);

dt = MX.sym('t',1); %MX.sym('dt',1); % sample time
t = MX.sym('t',1); % purely exist because ode45 needs it. It is not used otherwise

%% casadi-lize all the variables in the computation, e.g., system state
dXdt = MX.sym('dXdt',dim_state);  % system state derivatives
X = MX.sym('X',dim_state);  % system state

%% theta is the collection of controller parameters 
theta = MX.sym('theta',dim_controllerParameters);

%% define the control input
u = MX.sym('u',dim_control);

%% define the dynamics (discretization via Forward Euler or Runge Kutta)
dynamics = X + dt *[(1/parameters(3)) * u - (1/parameters(3)) * T_fm - (1/(parameters(5)*parameters(3))) * T_l
                    X(1)
                    -(1/parameters(4)) * T_fl + (1/parameters(4)) * T_l
                    X(3)];

%% compute the control action, denoted by h
h = controller(X,dXdt,err,theta,prev_P,dt,parameters,err_m_sum);

%% generate jacobians
grad_f_X = jacobian(dynamics,X);
grad_f_u = jacobian(dynamics,u);
grad_h_X = jacobian(h,X);
grad_h_theta = jacobian(h,theta);

%% function-lize the generated jacobians
% inputs_f and inputs_h denote the input arguments to the dynamics and controller h, respectively
grad_f_X_fcn = Function('grad_f_X_fcn',{X,u,dt,parameters},{grad_f_X});
grad_f_u_fcn = Function('grad_f_u_fcn',{X,u,dt,parameters},{grad_f_u});
grad_h_X_fcn = Function('grad_h_X_fcn',{X,dXdt,err,theta,prev_P,dt,parameters,err_m_sum},{grad_h_X});
grad_h_theta_fcn = Function('grad_h_theta_fcn',{X,dXdt,err,theta,prev_P,dt,parameters,err_m_sum},{grad_h_theta});

%% generate mex functions
opts = struct('main', true,...
              'mex', true);
% 
mkdir mex
cd('./mex');
grad_f_X_fcn.generate('grad_f_X_fcn.c',opts);
grad_f_u_fcn.generate('grad_f_u_fcn.c',opts);
grad_h_X_fcn.generate('grad_h_X_fcn.c',opts);
grad_h_theta_fcn.generate('grad_h_theta_fcn.c',opts);

mex grad_f_X_fcn.c -largeArrayDims
mex grad_f_u_fcn.c -largeArrayDims
mex grad_h_X_fcn.c -largeArrayDims
mex grad_h_theta_fcn.c -largeArrayDims
cd('../')
