% Use this script to run the simulation with DiffTune

close all;
clear all;

addpath('mex/');
import casadi.*

%% define the dimensions
dim_state = 4; % dimension of system state
dim_control = 1;  % dimension of control inputs
dim_controllerParameters = 3;  % dimension of controller parameters

%% Define simulation parameters (e.g., sample time dt, duration, etc)
dt = 0.0001;
time = 0:dt:0.5;
maxtime = dt*10000;

%% Initialize controller gains (must be a vector of size dim_controllerParameters x 1)
% Kp_m = 0.9;
% Ki_m = 16;
% Kp_l = 9;

Kp_m = 1;
Ki_m = 1;
Kp_l = 1;

theta = [Kp_m;Ki_m;Kp_l]

Xref = zeros(dim_state,1);
Xref_storage = zeros(dim_state,1);
prev_P = 0;
err_m_sum = 0;
dXdt = zeros(dim_state,1);
err_hist = [];

N=1;
Ks=32.979;
Ds=0.1471;
T_Cm = 0.0223;
J_l = 8.31e-4;
J_m = J_l;
omega_s=0.1;
beta_m = 0.0028;
beta_l = 0.0034;

parameters = [];
parameters(1) = Ks;
parameters(2) = Ds;
parameters(3) = J_m;
parameters(4) = J_l;
parameters(5) = N;
parameters(6) = T_Cm;
parameters(7) = beta_m;
parameters(8) = beta_l;

%% Define desired trajectory if necessary

%% Initialize variables for DiffTune iterations
learningRate = 2;
maxIterations = 2;
itr = 0;

loss_hist = [];  % storage of the loss value in each iteration
param_hist = []; % storage of the parameter value in each iteration
gradientUpdate = zeros(dim_controllerParameters,1); % define the parameter update at each iteration

%% DiffTune iterations
while (1)
    itr = itr + 1;

    % initialize state
    X_storage = zeros(dim_state,1);
    % initialize sensitivity
    dx_dtheta = zeros(dim_state,dim_controllerParameters);
    du_dtheta = zeros(dim_control,dim_controllerParameters);

    % initializegradient of loss
    theta_gradient = zeros(1,dim_controllerParameters);

    % initialize reference state if necessary

    for k = 1:length(time)-1
        % load current state and current reference
        X = X_storage(:,end);
        Xref = Xref_storage(:,end);
 
        % compute the control action (of dimension dim_control x 1)
        err = Xref - X;
        err_hist = [err_hist err(4)];
        [prev_P,u,err_m] = controller(X,dXdt,err,theta,prev_P,dt,parameters,err_m_sum);
        err_m_sum = err_m_sum + err_m*dt;
        % compute the sensitivity 
        [dx_dtheta, du_dtheta] = sensitivityComputation(dx_dtheta,X,u,dt,parameters,dXdt,err,theta,prev_P,err_m_sum);
        
        % accumulating the gradient of loss wrt controller parameters
        % you need to provide dloss_dx and dloss_du here
        dloss_dx = 2 * ([0, 0, 0, X(4)-Xref(4)]);
        dloss_du = 0;
        dloss_dx
        dx_dtheta
        theta_gradient = theta_gradient + dloss_dx * dx_dtheta;% + dloss_du * du_dtheta;

        % integrate the ode dynamics
        [~,sold] = ode45(@(t,X) dynamics(t,X,u,parameters),[time(k) time(k+1)],X);
        X_storage = [X_storage sold(end,:)'];

        Xref_storage = [Xref_storage [0,0,0,10]'];
    end
%     plot(err_hist,'Linewidth',3)
%     hold on
%     plot(X_storage(1,:),'Linewidth',3)
%     plot(X_storage(4,:),'Linewidth',3)
%     grid on
%     legend("err","motor vel","pos   ")
    % loss is the squared norm of the position tracking error
    loss = err_hist.^2;
    loss_hist = [loss_hist loss];

    % update the gradient
    gradientUpdate = - learningRate * theta_gradient;

    % sanity check
    if isnan(gradientUpdate)
       fprintf('gradient is NAN. Quit.\n');
%        break;
    end
   
    % gradient descent
    theta_gradient
    theta = theta + gradientUpdate'

    % projection of all parameters to the feasible set
    % the feasible set of parameters in this case is greater than 0.1
    % if any(theta < 0.1)
    %    neg_indicator = (theta < 0.1);
    %    pos_indicator = ~neg_indicator;
    %    theta_min = 0.1*ones(4,1);
    %    theta = neg_indicator.*theta_min + pos_indicator.*theta;
    % end

    % store the parameters
    % param_hist = [param_hist theta];

    % terminate if the total number of iterations is more than maxIterations
    if itr >= maxIterations
       break;
    end
end


%% plot trajectory

%% Debug session
% check_dx_dtheta = sum(isnan(dx_dtheta),'all');
% check_du_dtheta = sum(isnan(du_dtheta),'all');
