% This function defines how the sensitivity propgation runs at each sample time
% The inputs_f and inputs_h are of the same form to those used in templateAutoGeneration.m

function [dXdphi,dudphi] = sensitivityComputation(sensitivity,X,u,dt,parameters,dXdt,err,theta,prev_P,err_m_sum)

% evaluate the Jacobians
dfdX = grad_f_X_fcn(X,u,dt,parameters);
dfdX = full(dfdX);

dfdu = grad_f_u_fcn(X,u,dt,parameters);
dfdu = full(dfdu)

dhdX = grad_h_X_fcn(X,dXdt,err,theta,prev_P,dt,parameters,err_m_sum);
dhdX = full(dhdX);

dhdtheta = grad_h_theta_fcn(X,dXdt,err,theta,prev_P,dt,parameters,err_m_sum);
dhdtheta = full(dhdtheta)

%% assemble the Jacobians to compute the sensitivity
dXdphi = (dfdX + dfdu * dhdX) * sensitivity + dfdu * dhdtheta;
dudphi = dhdX * sensitivity + dhdtheta;

% end
