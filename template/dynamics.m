% Define the system dynamics (continuous time) here.
% It will be used in propagating the continuous dynamics.

% See original equation for dynamics in "Adaptive and sliding mode friction-resilient machine tool positioning – Cascaded control revisited" and "High Accuracy Nonlinear Control and Estimation for Machine Tool Systems" by Papageorgiou et. al.

function dXdt = dynamics(t,X,u,parameters)
    dXdt = zeros(4,1);

    omega_m = X(1);
    theta_m = X(2);
    omega_l = X(3);
    theta_l = X(4);

    K_s = parameters(1);
    D_s = parameters(2);
    J_m = parameters(3);
    J_l = parameters(4);
    N = parameters(5);
    T_Cm = parameters(6);
    beta_m = parameters(7);
    beta_l = parameters(8);

    T_l = K_s * (1/N * theta_m - theta_l) + D_s * (1/N * omega_m - omega_l);

    T_fm = T_Cm * sign(omega_m) + beta_m * omega_m;
    T_fl = T_Cm * sign(omega_l) + beta_l * omega_l;;

    d_omega_m = (1/J_m) * u - (1/J_m) * T_fm - (1/(N*J_m)) * T_l;
    d_theta_m = omega_m;

    d_omega_l = -(1/J_l) * T_fl + (1/J_l) * T_l;
    d_theta_l = omega_l;

    dXdt = [d_omega_m; d_theta_m; d_omega_l; d_theta_l];
end
